\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Model}{5}{0}{2}
\beamer@sectionintoc {3}{Space-time discretization}{7}{0}{3}
\beamer@subsectionintoc {3}{1}{L-scheme + Discontinuous Galerkin FE in time}{11}{0}{3}
\beamer@subsectionintoc {3}{2}{Newtons method + Discontinuous Galerkin FE in time}{20}{0}{3}
\beamer@sectionintoc {4}{Numerical methods}{22}{0}{4}
\beamer@sectionintoc {5}{Remarks and conclusions}{29}{0}{5}
