% ========================================================
% A beamer presentation theme for the Porous Media Group of 
% the Department of Mathematics, University of Bergen
% 
% Conceptual design by J.M. Sargado and A. Budisa
% Last modified: 27 May 2018
% ========================================================
%
% Usage:
%
% To use this beamer theme, include the line
%
% \usetheme{PMGpresentation}
%
% in the preamble of the main tex file. The template 
% comes with two color themes that are derived from 
% the dominant colors of the Porous Media Group logo.
% Optional arguments are:
%
% 1. Blue - activates the blue color theme.
%
% 2. MuseumFront - changes the image in the title frame
%					to a front view of the Bergen Museum.
%
% 3. MuseumBack - changes the image in the title frame
%                   to a back view of the Bergen Museum.
%
% 4. Roar - changes the image in the title frame to a picture
%                   focusing on the lion sculpture in front of the
%                   Bergen Museum.
%
% 5. sectionframes - will generate a separate frame at the
%                  beginning of each section containing the section
%                  title.
%
% 6. usebiblatex - should be passed as an argument when
%			       biblatex is to used for generating
%                  bibliographies.
%
% Elements:
%
% The poster title, authors, institute and date should be
% defined in the preamble of the main tex file using
% the standard commands:
%
% \title{title-in-header}
% \author[short-author]{author-names}
% \institute[short-inst]{institute-names}
% \date{user-supplied-info}
%
% 
%
% Change log:
%
% 1.00 - Initial release
% ========================================================

\mode<presentation>
\usepackage{pgf}
\usepackage{graphicx}
\usepackage{tcolorbox}
\usepackage{changepage}

% -------------------------------------------------------------------------------------------------------------------------
%                                                         SIZES AND MARGINS
% -------------------------------------------------------------------------------------------------------------------------

\geometry{paperwidth = 168mm, paperheight = 126mm}
\setbeamersize{text margin left = 0.03\paperwidth, text margin right = 0.03\paperwidth}

% -------------------------------------------------------------------------------------------------------------------------
%                                                                   OPTIONS
% -------------------------------------------------------------------------------------------------------------------------

% Color definitions
\definecolor{PMG_Red}{HTML}{AF272F}
\definecolor{PMG_Blue}{HTML}{004C97}
\definecolor{PMG_TheoremBodyR}{HTML}{B26428}
\definecolor{PMG_TheoremBodyB}{HTML}{009C65}
\definecolor{PMG_ExampleBodyR}{HTML}{2B9221}
\definecolor{PMG_ExampleBodyB}{HTML}{E98A00}
\definecolor{PMG_Gray}{HTML}{F1F1F1}

\newcommand{\titlePageLogo}{ThemeFiles_PMGpresentation/UIB_TitleLogo_Red}
\newcommand{\frameLogo}{ThemeFiles_PMGpresentation/UIB_Logo_HighRes_Red}
\newcommand{\titlePagePicture}{ThemeFiles_PMGpresentation/TitlePic_MatNat}
\newcommand{\themeColor}{PMG_Red}
\newcommand{\otherColor}{PMG_Blue}
\newcommand{\theoremBodyColor}{PMG_TheoremBodyR}
\newcommand{\exampleBodyColor}{PMG_ExampleBodyR}

\newif\if@usebiblatex
\newif\if@useSectionFrames

\DeclareOption{Blue}
{
	\renewcommand{\themeColor}{PMG_Blue}
	\renewcommand{\otherColor}{PMG_Red}
	\renewcommand{\titlePageLogo}{ThemeFiles_PMGpresentation/UIB_TitleLogo_Blue}
	\renewcommand{\frameLogo}{ThemeFiles_PMGpresentation/UIB_Logo_HighRes_Blue}
	\renewcommand{\theoremBodyColor}{PMG_TheoremBodyB}
	\renewcommand{\exampleBodyColor}{PMG_ExampleBodyB}
}
\DeclareOption{MuseumFront}
{
	\renewcommand{\titlePagePicture}{ThemeFiles_PMGpresentation/TitlePic_MuseumFront}
}
\DeclareOption{MuseumBack}
{
	\renewcommand{\titlePagePicture}{ThemeFiles_PMGpresentation/TitlePic_MuseumBack}
}
\DeclareOption{Roar}
{
	\renewcommand{\titlePagePicture}{ThemeFiles_PMGpresentation/TitlePic_MatNat}
}
\DeclareOption{Bergen}
{
	\renewcommand{\titlePagePicture}{ThemeFiles_PMGpresentation/TitlePic_MuseumLion}
}
\DeclareOption{sectionframes}
{
	\@useSectionFramestrue
}
\DeclareOption{usebiblatex}
{
	\@usebiblatextrue
}

\ProcessOptions\relax

\if@usebiblatex
\RequirePackage[citestyle=numeric, citetracker=true, bibstyle=numeric-comp, sortcites=true, block=space, giveninits]{biblatex}
\AtEveryBibitem
{
	\clearlist{address}
	\clearfield{date}
	\clearfield{eprint}
	\clearfield{isbn}
	\clearfield{issn}
	\clearlist{location}
	\clearfield{month}
	\clearfield{series}
	\clearfield{doi}
	\clearfield{url}
	\clearfield{eid}
	
	% Remove publisher and editor except for books
	\ifentrytype{book}{}
	{
		\clearlist{publisher}
		\clearname{editor}
	}
}
\else
% Remove line feeds after title, location and note fields
% when using Bibtex to generate the bibliography
\setbeamertemplate{bibliography entry title}{}
\setbeamertemplate{bibliography entry location}{}
\setbeamertemplate{bibliography entry note}{}
\fi

% -----------------------------------------
% 			COLOR ASSIGNMENTS
% -----------------------------------------

% This sets the colour of the title of the presentation and titles of all the slides
\setbeamercolor{frametitle}{fg = black}

% In case you choose to display the Table of Contents, or the Outline slide.
\setbeamercolor{section in toc}{fg = black}
\setbeamercolor{section in toc shaded}{fg = black}

% The colour of all the items, subitems and and subsubitems are set to black.
\setbeamercolor{item}{fg = black}
\setbeamercolor{subitem}{fg = black}
\setbeamercolor{subsubitem}{fg = black}

% Color for table and figure captions
\setbeamercolor{caption}{fg = black}
\setbeamercolor{caption name}{fg = black}

% Background color of the slides
\setbeamercolor{background canvas}{bg = white}

% Standard block (Theorems)
\setbeamercolor{block title}{fg = white, bg = \themeColor}
\setbeamercolor{block body}{bg =\theoremBodyColor!30!white}

% Example block
\setbeamercolor{block title example}{fg = white, bg = \themeColor}
\setbeamercolor{block body example}{bg = \exampleBodyColor!30!white}

% Color of normal text
\setbeamercolor{normal text}{fg = black}

\setbeamercolor{TitlePageTextArea}{fg=black, bg=PMG_Gray}
\setbeamercolor{BottomRule}{bg=\themeColor}

% Color for enumerate
\setbeamercolor{item projected}{fg=black, bg=PMG_Gray}

% -----------------------------------------
% 			    FONT THEMES
% -----------------------------------------

\usefonttheme{professionalfonts}

% Font for the presentation title
\setbeamerfont{title}{size=\huge, series=\bfseries, shape=\upshape}
\setbeamerfont{author}{size=\large, series=\mdseries, shape=\slshape}
\setbeamerfont{institute}{size=\footnotesize, series=\mdseries, shape=\slshape}
\setbeamerfont{date}{size=\footnotesize, series=\mdseries, shape=\upshape}
\setbeamerfont{frametitle}{size=\Large, series=\bfseries}

\setbeamerfont{footnote}{size=\tiny}


% -----------------------------------------
% 			     INNER THEME
% -----------------------------------------

\useinnertheme{default}
\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{enumerate items}[default]

% Frame titles
% ----------------------------------------------------------------------------------------------------------
\setbeamercolor{frametitle_color}{fg = black, bg = PMG_Gray}
\setbeamertemplate{frametitle}
{
	\vskip-0.005\paperheight
	\begin{beamercolorbox}[wd=\paperwidth, ht=0.08\paperheight]{frametitle_color} 
		\vbox to 0.075\paperheight {\vfil \hskip0.03\paperwidth \insertframetitle \vfil}
	\end{beamercolorbox}
}

% Foot line
% ----------------------------------------------------------------------------------------------------------
\setbeamercolor{footer}{fg = black, bg = PMG_Gray}
\setbeamertemplate{footline}
{
	\offinterlineskip
	% Bottom Rule
	 \begin{beamercolorbox}[wd=\paperwidth,ht=0.005\paperwidth,dp=0pt]{BottomRule}
	 \end{beamercolorbox}
	
    \begin{beamercolorbox}[wd=\paperwidth,ht=0.029\paperheight,sep=0.005\paperheight]{footer}
        \usebeamerfont{structure}
	        \tiny
            \hspace*{1em}
            \hspace*{-1em} \insertframenumber/\inserttotalframenumber
            \hspace*{1pt} $\vert$ 
            \ifx\beamer@shortitle\@empty
	            \insertshortauthor
	        \else
		        \insertshortauthor \hskip0.02em, \insertshorttitle
		    \fi
		    \hspace*{1pt} $\vert$ 
		    \insertshortdate
    \end{beamercolorbox}
    
    % UiB logo at top right
    \vskip-0.05\paperheight
    \begin{beamercolorbox}[wd=\paperwidth,ht=0.05\paperheight]{}
    	\hfill \includegraphics[height=0.1\paperheight]{\frameLogo} \hspace*{0.02\paperheight} \vskip0.9\paperheight
    \end{beamercolorbox}
}

% Remove navigation symbols
\setbeamertemplate{navigation symbols}{}

% Title page
% ----------------------------------------------
\setbeamertemplate{title page}
{
	\offinterlineskip
	\vskip-0.52\paperheight
	
	% header / picture
	 \begin{beamercolorbox}[wd=0.94\paperwidth,ht=0.53\paperheight]{}
	 	\hfill \includegraphics[height=0.08\paperheight]{ThemeFiles_PMGpresentation/PMG_Logo} \\[0.8em]
	 	\includegraphics[width=0.94\paperwidth]{\titlePagePicture}
	 \end{beamercolorbox}%
	 
	 % Title area
	 \begin{beamercolorbox}[wd=0.94\paperwidth, ht=0.4\paperheight, sep=0.01\paperwidth]{TitlePageTextArea}
	 	\vbox to 0.4\paperheight{
	 		\vfill \vfill \par
%		 	\setlength{\parskip}{0.8em}
		 	\setlength{\lineskip}{0.3em}
		 	\usebeamerfont{title} \inserttitle \par
		 	\vfill
		 	\setlength{\lineskip}{0em}
		 	\usebeamerfont{author}\insertauthor \\[0.3em]
		 	\usebeamerfont{institute}\insertinstitute\par
		 	\vfill
		 	\usebeamerfont{date}\insertdate \par
		 }
	 \end{beamercolorbox}%	  
	
	 % Bottom line
	 \begin{beamercolorbox}[wd=0.94\paperwidth,ht=0.015\paperwidth,dp=0pt]{BottomRule}%
	 \end{beamercolorbox}
	
	 \vskip-1.05\paperheight
	 % UiB logo
	 \begin{beamercolorbox}[wd=\paperwidth,ht=0.3\paperheight]{}
	 	\includegraphics[width=0.45\paperwidth]{\titlePageLogo}\hfill
	 \end{beamercolorbox}	 
}

% Section frames
\if@useSectionFrames
\AtBeginSection[]
{
	{
		\setbeamercolor{background canvas}{bg = PMG_Gray}
		\setbeamercolor{sectionTitle}{fg = white, bg = \themeColor}
		\setbeamertemplate{footline}{}
		\begin{frame}[plain,noframenumbering,t]
			\begin{adjustwidth}{-0.03\paperwidth}{-0.03\paperwidth}
				\vskip0.05\paperheight
				\begin{tcolorbox}[colback = \themeColor, colframe = \themeColor, arc=0pt,outer arc=0pt, hbox]
					\textcolor{white}{\Large \bfseries \insertsectionhead}
				\end{tcolorbox}
			\end{adjustwidth}
		\end{frame}
	}
}
\fi

% Do not include appendix frames in slide count
\input{ThemeFiles_PMGpresentation/appendixnumberbeamer.sty}

\mode<all>
