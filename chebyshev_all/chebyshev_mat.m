% Create the matrix of differentiation on the space of polynomials of
% degree <=n with basis the Chebyshev polynomials
function v = chebyshev_mat(n)
v = zeros(n);
for i = 1:n
    if mod(i,2) == 1
        v(2*(1:((i-1)/2)),i) = 2*(i-1);
    else
        v(1,i) = i-1;
        v(2*(2:((i)/2))-1,i) = 2*(i-1);
    end
end