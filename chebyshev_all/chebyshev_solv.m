% Approximate the solution to the equation u''+u = f
% Inputs: n the number (even) coefficients to use
%         v the chebyshev coefficients of f
function u = chebyshev_solv (n, v)
% Set up D^2_{ev}
D = chebyshev_mat(2*n);
D2 = D*D;
D2ev = D2(2*(1:n)-1,2*(1:n)-1);

% Set up I_{ev}
I = eye(n);

%(D^2_{ev}+I_{ev})u = v (in this basis, so invert this)
warning off;
u = lsqlin((I+D2ev),v,[],[],ones(n),zeros(n,1),[],[],[],optimset('Display','off'));
warning on;