% Evaluate a Chebyshev expansion at a set of points
% Inputs: v = vector of coefficients for expansion
%         x = range which you wish to evaluate on

function y = evaluate_cheb(v, x)
[m n] = size(v);
y = zeros(size(x));
% v(1) is the constant coefficient - add to everything
y = v(1);
for i = 2:m
    % Create the polynomial with roots at in the correct place with the
    % right leading coefficient
    chebyshev_points = cos(pi/2*(2*(1:(i-1))-1)/(i-1));
    p = 2^(i-2)*poly(chebyshev_points);
    % Evaluate this function and combine it with the coefficient.
    y = y + v(i)*polyval(p,x);
end