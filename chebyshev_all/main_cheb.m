hold on;


for n = 1:15
    v = zeros(n,1);
    v(1) = 1;
    u = chebyshev_solv(n,v);
    u = pad_ev(u);
    x = -1:0.01:1;
    plot(x,evaluate_cheb(u,x));
end
plot(x,1-cos(x)/cos(1),'r');
hold off;