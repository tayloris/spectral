function v = pad_ev ( u )
[m n] = size(u);
v = zeros(2*m,1);
v(2*(1:m)-1) = u';