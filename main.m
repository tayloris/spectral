

N = 16;

x = linspace(2*pi/N,2*pi,N); 

f = (sin(x).^2-cos(x)) .*exp(cos(x));

f_hat = fft(f);  % Fourier Transform of f

ik = 1i*[0:N/2 -N/2+1:-1];   %i * wave number vector (matlab ordering) 
   
   ik2 = ik.*ik;                %multiplication factor for second derivative
   ii = find(ik~= 0);           %indices where ik is nonzero
  ik2inverse = ik2;             %initialize zeros in same locations as in ik2
  ik2inverse(ii) = 1./ik2(ii);  %multiplier factor to solve u'' = f
  u_hat = ik2inverse.*f_hat;
  
  u_N_i =@(y) sum(u_hat.*exp(ik*y));
  
  for i =1:length(x)
      u_N(i) = (1.0/N)*u_N_i(x(i));
  end
  
  u = real(ifft(u_hat));   %imaginary parts should be round off level
  
  
  plot(x,u,'k-o')
 
  v = exp(cos(x));
    u2 = u+v(1)-u(1);

  plot(x,u,'k-o',x,v,'r-*',x,u2,'k-o',x+2*pi/N,u_N,'b-s')
  
  legend('Book sol','Real sol','Book solv2', 'My solution')
  
  norm(u2-v, inf)